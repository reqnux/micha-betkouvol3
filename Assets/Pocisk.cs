﻿using UnityEngine;
using System.Collections;

public class Pocisk : MonoBehaviour {

	private Gracz wlasciciel;
	public float predkoscPocisku;

	void Start() {
		GetComponent<Rigidbody> ().velocity = Vector3.forward * predkoscPocisku;
		Destroy(gameObject,2f);
	}
	void OnCollisionEnter(Collision col) {
		if(col.gameObject.tag == "Przeciwnik") {
			if(wlasciciel != null) {
				col.gameObject.GetComponent<Przeciwnik>().zadajObrazenia(wlasciciel.obrazenia);
			}
			Destroy(gameObject);
		}

	}

	public Gracz Wlasciciel {
		set{wlasciciel = value;}
	}
}
