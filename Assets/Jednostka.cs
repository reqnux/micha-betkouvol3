﻿using UnityEngine;
using System.Collections;

public class Jednostka : MonoBehaviour {

	public int aktualneZycie;
	public int maxZycie;
	public int obrazenia;
	public float predkoscRuchu;

	public void zadajObrazenia(int aObrazenia) {
		aktualneZycie -= aObrazenia;
		if (aktualneZycie <= 0) {
			smierc();
		}
	}
	private void smierc() {
		Destroy(gameObject);
	}


	public int Obrazenia {
		get{return obrazenia;}
		set{obrazenia = value;}
	}
	public float PredkoscRuchu {
		get{return predkoscRuchu;}
		set{predkoscRuchu = value;}
	}
}
