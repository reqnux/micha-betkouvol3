﻿using UnityEngine;
using System.Collections;

public class Kamera : MonoBehaviour {

	public Transform gracz;
	public float offsetX;
	public float offsetY;
	public float offsetZ;


	void LateUpdate () {
		transform.position = gracz.position + new Vector3 (offsetX, offsetY, offsetZ);

	}
}
