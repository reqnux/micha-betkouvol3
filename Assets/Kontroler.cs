﻿using UnityEngine;
using System.Collections;

public class Kontroler : MonoBehaviour {


	private Gracz gracz;
	// Use this for initialization
	void Start () {
		gracz = GetComponent<Gracz> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp(KeyCode.Mouse0)) {
			gracz.strzal();
		}

		Vector3 ruch = new Vector3(Input.GetAxis("Horizontal"),0,Input.GetAxis("Vertical"));
		GetComponent<Rigidbody>().velocity = ruch.normalized * gracz.PredkoscRuchu;
	}
}
