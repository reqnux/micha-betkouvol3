﻿using UnityEngine;
using System.Collections;

public class Gracz : Jednostka {

	public Pocisk prefabPocisk;
	private Transform spawnPociskow;

	void Start() {
		spawnPociskow = transform.FindChild("spawnPociskow");
	}

	public void strzal() {
		Pocisk p = Instantiate(prefabPocisk,spawnPociskow.position,Quaternion.identity) as Pocisk;
		p.Wlasciciel = this;
	}

}
